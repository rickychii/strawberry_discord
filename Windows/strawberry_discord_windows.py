#!/usr/bin/env python3

#RICKYCHII - 2023/2024

import struct
import sys
import os
import datetime
import logging
import time
import glob
from urllib.parse import unquote

import pypresence

DETAILS_STRING = "Current Song: 🎵 {xesam-title}"
CLIENT_ID = 994206319504539748

local_appdata_path = os.environ['LOCALAPPDATA'] + "\\Strawberry\\Strawberry\\cache\\moodbar\\"

class PresenceUpdater:
    def __init__(self):

        #READ EXCLUDE PATH
        self.excludepath = ""
        
        script_path = os.path.abspath(__file__)
        script_dir = os.path.dirname(script_path) + "\\"

        with open(script_dir + "excludepath.txt", 'r') as file:
            for line in file:
                line = line.strip()
                self.excludepath = line
                break

        self.last_read_time = datetime.datetime(2015,1,1,0,0,0)
        self.last_folder = ""
        self.last_read_file_time = datetime.datetime(2015,1,1,0,0,0)
        self.last_file = ""

        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.logger.info("Initializing.")

        self.client = pypresence.Presence(CLIENT_ID)

    def run(self):
        self.logger.info("Connecting to Discord.")
        self.client.connect()
        self.logger.info("Preparing the enveironment...")
        self.resetFolderContent()
        time.sleep(2)
        self.logger.info("Runnin main cycle.")
        self.update_loop()


    def resetFolderContent(self):
        data8path = local_appdata_path + "data8\\"
        
        if(os.path.isdir(data8path)):

            #0-9
            for i in range (0,9):
                
                if(os.path.isdir(data8path + str(i))):
                    files = glob.glob(data8path + str(i) + "\\*")
                    for f in files:
                        os.remove(f)

            #a-f
            if(os.path.isdir(data8path + "a")):
                files = glob.glob(data8path + "a\\*")
                for f in files:
                    os.remove(f)
            if(os.path.isdir(data8path + "b")):
                files = glob.glob(data8path + "b\\*")
                for f in files:
                    os.remove(f)
            if(os.path.isdir(data8path + "c")):
                files = glob.glob(data8path + "c\\*")
                for f in files:
                    os.remove(f)
            if(os.path.isdir(data8path + "d")):
                files = glob.glob(data8path + "d\\*")
                for f in files:
                    os.remove(f)
            if(os.path.isdir(data8path + "e")):
                files = glob.glob(data8path + "e\\*")
                for f in files:
                    os.remove(f)
            if(os.path.isdir(data8path + "f")):
                files = glob.glob(data8path + "f\\*")
                for f in files:
                    os.remove(f)

    def update_loop(self):
        while True:

            directories = [d for d in os.listdir(local_appdata_path) if os.path.isdir(os.path.join(local_appdata_path, d))]
            if len(directories) == 0:
                self.logger.info("Please enable moodbar!")
                exit()

            subpath = local_appdata_path + directories[0] + "\\"
            subdirectories = [d for d in os.listdir(subpath) if os.path.isdir(os.path.join(subpath, d))]
            
            if(len(subdirectories) == 0):
                time.sleep(5)
                continue

            for sd in subdirectories:
                #GET LAST MODIFY DATE:
                timestamp = datetime.datetime.fromtimestamp(os.path.getmtime(subpath + sd))
                if timestamp > self.last_read_time:
                    self.last_read_time = timestamp
                    self.last_folder = sd


            subpath = subpath + self.last_folder + "\\"
            #GET ALL FILES:
            files = [f for f in os.listdir(subpath) if os.path.isfile(os.path.join(subpath, f))]
            for fd in files:
                #GET LAST MODIFY DATE:
                timestamp = datetime.datetime.fromtimestamp(os.path.getmtime(subpath + fd))
                if timestamp > self.last_read_file_time:
                    self.last_read_file_time = timestamp
                    self.last_file = fd

            if(len(os.listdir(subpath)) == 0):
                time.sleep(5)
                continue

            subpath = subpath + self.last_file
            song = ""
            with open(subpath, 'rb') as file:
                for line in file:
                    line = line.decode('utf-8', 'replace')

                    line = unquote(line)
                    index = -1

                    startindex = line.index(self.excludepath)

                    #MP3
                    if(".mp3" in line.lower()):
                        index = line.lower().rfind(".mp3") + 4
                    #WAV
                    if(".wav" in line.lower()):
                        index = line.lower().rfind(".wav") + 4
                    #FLAC
                    if(".flac" in line.lower()):
                        index = line.lower().rfind(".flac") + 5

                    song = line[startindex:index]
                    song = song.replace(self.excludepath, "")
                    song = song.strip()

                    #MAX 110 chars
                    if len(song) > 110:
                        song = song[-110:]
                    
                    song = DETAILS_STRING.replace("{xesam-title}",song)
                    break

            playback_status = song
            details = "[ on Windows 💻 Platform ]"

            time_start = None
            time_end = None

            self.logger.debug("Updating Discord.")
            self.client.update(state=playback_status,
                               details=details,
                               start=time_start,
                               end=time_end)
            time.sleep(5)

    def close(self):
        self.logger.info("Shutting down.")
        self.client.close()

if __name__ == '__main__':
    updater = PresenceUpdater()
    try:
        updater.run()
    finally:
        updater.close()
